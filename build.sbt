/*
 * Copyright © 2017-2022 Lyle Frost <lfrost@cnz.com>
 */

import org.irundaia.sbt.sass.ForceScss

ThisBuild / organization := "com.cnz"
ThisBuild / scalaVersion := "2.13.10"
ThisBuild / version      := "1.2.0"

maintainer := "lfrost@cnz.com"

val gitBaseUrl = settingKey[String]("Git base URL")
gitBaseUrl := "https://gitlab.com/lfrost"

lazy val root = (project in file(".")).
    enablePlugins(PlayScala, SbtWeb).
    settings(
        name := "play-cnz-example",
        resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
        libraryDependencies ++= Seq(
            filters,
            guice,
            ws,
            "com.cnz.play"           %% "play-cnz"           % "0.2.5-SNAPSHOT",
            "org.scalatestplus.play" %% "scalatestplus-play" % "5.1.0" % Test
        ),
        // https://docs.scala-lang.org/overviews/compiler-options/
        scalacOptions ++= Seq(
            // Standard Settings
            "--deprecation",            // Emit warning and location for usages of deprecated APIs.
            "--encoding", "UTF-8",      // Specify character encoding used by source files.
            "--explain-types",          // Explain type errors in more detail.
            "--feature",                // Emit warning and location for usages of features that should be imported explicitly.
            "--release:11",             // Target platform for object files. (8,9,10,11,12,13,14,15,16,[17])
            "--unchecked",              // Enable additional warnings where generated code depends on assumptions.

            // Advanced Settings

            // Warning Settings
            "-Ywarn-dead-code",         // Warn when dead code is identified.
            "-Ywarn-value-discard",     // Warn when non-Unit expression results are unused.
            "-Ywarn-numeric-widen",     // Warn when numerics are widened.
            "-Ywarn-unused:_",          // Enable or disable specific unused warnings: _ for all, -Ywarn-unused:help to list choices.
            "-Ywarn-extra-implicit",    // Warn when more than one implicit parameter section is defined.
            "-Xlint:_"                  // Enable or disable specific warnings: _ for all, -Xlint:help to list choices.
        ),
        Compile / doc / scalacOptions ++=
            Opts.doc.title(name.value)                                                                                             ++
            Opts.doc.sourceUrl(gitBaseUrl.value + "/" + name.value + "/tree/" + git.gitCurrentBranch.value + "€{FILE_PATH}.scala") ++
            Seq("-sourcepath", baseDirectory.value.getAbsolutePath),
        fork := true,
        // Enable and configure sbt-digest.
        pipelineStages := Seq(digest),
        DigestKeys.algorithms += "sha1",
        // Configure Sass (https://github.com/irundaia/sbt-sassify#options).
        SassKeys.syntaxDetection := ForceScss,
        play.sbt.routes.RoutesKeys.routesImport ++= Seq(
            "java.util.UUID",
            "com.cnz.play.mvc.QueryFilter",
            "com.cnz.play.mvc.QueryPage",
            "com.cnz.play.mvc.QuerySort"
        ),
        TwirlKeys.templateImports ++= Seq(
            "com.cnz.play.mvc.QueryFilter",
            "com.cnz.play.mvc.QueryPage",
            "com.cnz.play.mvc.QuerySort"
        ),
        // Exclude generated files from coverage report.
        coverageExcludedPackages := ";controllers\\.Reverse.*;controllers\\.javascript\\.Reverse.*;router\\.Routes.*"
    )
