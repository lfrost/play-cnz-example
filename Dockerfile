#
# Copyright © 2017-2022 Lyle Frost <lfrost@cnz.com>
#

FROM    debian:bullseye-slim
ARG     APP_NAME=play-cnz-example
ARG     APP_VERSION=1.2.0
ARG     BUILD_DATE
ARG     VCS_REF

# See http://label-schema.org/.
LABEL   org.label-schema.schema-version="1.0"                                                       \
        org.label-schema.name=$APP_NAME                                                             \
        org.label-schema.build-date=$BUILD_DATE                                                     \
        org.label-schema.vcs-ref=$VCS_REF                                                           \
        org.label-schema.vcs-url="https://github.com/lfrost/play-cnz-example"                       \
        org.label-schema.description="This image is used to launch a play-cnz-example server."      \
        org.label-schema.docker.debug="docker exec -it CONTAINER-ID /bin/bash"

ENV     APP_NAME=$APP_NAME          \
        APP_VERSION=$APP_VERSION    \
        APP_USER=play               \
        APP_HOME=/home/play         \
        HTTP_PORT=9000              \
        JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64"

EXPOSE  $HTTP_PORT

# Set shell to bash.
SHELL   ["/bin/bash", "-c"]

# Run commands requiring root.
# The mkdir is a workaround for an OpenJDK installation error.
RUN     set -fu -o pipefail                     && \
        mkdir -p /usr/share/man/man1            && \
        export DEBIAN_FRONTEND=noninteractive   && \
        apt-get update                          && \
        apt-get install --assume-yes apt-utils  && \
        apt-get upgrade --assume-yes            && \
        apt-get install --assume-yes               \
            openjdk-11-jre-headless                \
            unzip                               && \
        useradd --comment 'Play' --create-home --home-dir "$APP_HOME" --shell '/bin/bash' "$APP_USER"

# Switch to play user and finish.
USER    $APP_USER:$APP_USER
WORKDIR $APP_HOME
ADD     --chown=play:play target/universal/$APP_NAME-$APP_VERSION.zip $APP_NAME-$APP_VERSION.zip
RUN     set -fu -o pipefail                 && \
        unzip $APP_NAME-$APP_VERSION.zip    && \
        mv "$APP_NAME-$APP_VERSION" "$APP_NAME"

# Start the Play application.
CMD ["play-cnz-example/bin/play-cnz-example", "-Dhttp.port=9000"]
