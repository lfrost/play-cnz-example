%YAML 1.2
---
openapi : 3.0.3
info :
    title          : play-cnz-example API
    description    : The API for play-cnz-example
    termsOfService : 'https://www.example.com/terms/'
    version        : 1.0.0
    contact :
        name  : CNZ Support
        url   : 'https://www.example.com/support/'
        email : support@example.com
    license :
        name : Apache 2.0
        url  : 'https://gitlab.com/lfrost/play-cnz-example/blob/master/LICENSE'
servers :
-   url         : 'http://localhost:9000'
    description : Local development server
paths :
    /api/Person :
        get :
            operationId : listPersons
            summary     : List persons
            description : Returns list of persons
            tags :
            -   person
            parameters :
            -   name        : 'filter[*]'
                in          : query
                description : search across all fields
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.nameLast]'
                in          : query
                description : search for last name
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.nameFirst]'
                in          : query
                description : search for first name
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.nameMiddle]'
                in          : query
                description : search for middle name
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.nameSuffix]'
                in          : query
                description : search for name suffix
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.email]'
                in          : query
                description : search for email
                required    : false
                explode     : true
                schema :
                    type   : string
                    format : email
            -   name        : 'filter[Person.phoneHome]'
                in          : query
                description : search for home phone
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.phoneMobile]'
                in          : query
                description : search for mobile phone
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.phoneWork]'
                in          : query
                description : search for work phone
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.address]'
                in          : query
                description : search for address
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.city]'
                in          : query
                description : search for city
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.countyCode]'
                in          : query
                description : search for county code
                required    : false
                explode     : true
                schema :
                    type : string
            -   name        : 'filter[Person.stateCode]'
                in          : query
                description : search for state code
                required    : false
                explode     : true
                schema :
                    $ref : '#/components/schemas/StateCode'
            -   name        : 'filter[Person.countryCode]'
                in          : query
                description : search for country code
                required    : false
                explode     : true
                schema :
                    $ref : '#/components/schemas/CountryCode'
            -   name        : 'filter[Person.postalCode]'
                in          : query
                description : search for postal code
                required    : false
                explode     : true
                schema :
                    $ref : '#/components/schemas/PostalCode'
            -   name        : 'page[number]'
                in          : query
                description : page number
                required    : false
                schema :
                    type    : integer
                    format  : int64
                    minimum : 1
            -   name        : 'page[size]'
                in          : query
                description : page size
                required    : false
                schema :
                    type    : integer
                    format  : int32
                    minimum : 4
            -   name        : sort
                in          : query
                description : sort order
                required    : false
                explode     : false
                schema :
                    type : array
                    items :
                        type : string
                        enum :
                        -   Person.nameLast
                        -   '-Person.nameLast'
                        -   Person.nameFirst
                        -   '-Person.nameFirst'
                        -   Person.nameMiddle
                        -   '-Person.nameMiddle'
                        -   Person.nameSuffix
                        -   '-Person.nameSuffix'
                        -   Person.email
                        -   '-Person.email'
                        -   Person.city
                        -   '-Person.city'
                        -   Person.countyCode
                        -   '-Person.countyCode'
                        -   Person.stateCode
                        -   '-Person.stateCode'
                        -   Person.countryCode
                        -   '-Person.countryCode'
                        -   Person.postalCode
                        -   '-Person.postalCode'
            responses :
                '200' :
                    description : OK
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/PersonListJson'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/PersonListJsonApi'
                '403' :
                    description : Forbidden
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/JsonError'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/JsonApiError'
                default :
                    description : Error
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/JsonError'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/JsonApiError'
    '/api/Person/{id}' :
        parameters :
        -   name        : id
            in          : path
            description : ID of person to find
            required    : true
            explode     : true
            schema :
                $ref : '#/components/schemas/Id'
        get :
            operationId : findPerson
            summary     : Find a person
            description : Returns a person
            tags :
            -   person
            responses :
                '200' :
                    description : OK
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/PersonGetJson'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/PersonGetJsonApi'
                '403' :
                    description : Forbidden
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/JsonError'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/JsonApiError'
                '404' :
                    description : Not found
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/JsonError'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/JsonApiError'
                default :
                    description : Error
                    content :
                        application/json :
                            schema :
                                $ref : '#/components/schemas/JsonError'
                        application/vnd.api+json :
                            schema :
                                $ref : '#/components/schemas/JsonApiError'
components :
    schemas :
        Id :
            description : ID
            type        : string
            format      : uuid
        CountryCode :
            description : ISO 3166-1 alpha-2 (upper case)
            type        : string
            example     : US
        PostalCode :
            description : Postal code
            type        : string
            example     : '45236'
        StateCode :
            description : ISO 3166-2 (upper case)
            type        : string
            example     : OH
        JsonError :
            type : array
            items :
                $ref : '#/components/schemas/JsonApiErrorItem'
        JsonApiError :
            type : object
            required :
            -   errors
            properties :
                jsonapi :
                    $ref : '#/components/schemas/JsonApiInfo'
                errors :
                    type : array
                    items :
                        $ref : '#/components/schemas/JsonApiErrorItem'
        JsonApiErrorItem :
            type : object
            properties :
                id :
                    description : a unique identifier for this particular occurrence of the problem
                    type        : string
                status :
                    description : the HTTP status code applicable to this problem
                    type        : string
                code :
                    description : an application-specific error code
                    type        : string
                title :
                    description : 'a short, human-readable summary of the problem that does not change from occurrence to occurrence of the problem, except for purposes of localization'
                    type        : string
                detail :
                    description : 'a human-readable explanation specific to this occurrence of the problem, except for purposes of localization'
                    type        : string
                source :
                    type : object
                    properties :
                        parameter :
                            description : a string indicating which URL query parameter caused the error
                            type        : string
                        pointer :
                            description : 'a JSON Pointer [RFC6901] to the associated entity in the request document [e.g. "/data" for a primary data object, or "/data/attributes/title" for a specific attribute]'
                            type        : string
        JsonApiInfo :
            type : object
            properties :
                version :
                    type : string
                    enum :
                    -   '1.0'
                meta :
                    type : object
        JsonApiLinks :
            type : object
            properties :
                self :
                    description : the link that generated the current response document
                    type        : string
                    format      : url
                first :
                    description : the first page of data
                    type        : string
                    format      : url
                last :
                    description : the last page of data
                    type        : string
                    format      : url
                prev :
                    description : the previous page of data
                    type        : string
                    format      : url
                next :
                    description : the next page of data
                    type        : string
                    format      : url
        PersonData :
            type     : object
            required :
            -   when-created
            -   name-last
            properties :
                when-created :
                    description : When created
                    type        : string
                    format      : date-time
                name-last :
                    description : Last name
                    type        : string
                    example     : Marlowe
                name-first :
                    description : First name
                    type        : string
                    example     : Philip
                name-middle :
                    description : Middle name
                    type        : string
                    example     : ''
                name-suffix :
                    description : Name suffix
                    type        : string
                    example     : 'Jr.'
                salutation :
                    description : Salutation
                    type        : string
                    example     : ''
                email :
                    description : Email address
                    type        : string
                    format      : email
                phone-home :
                    description : Home phone number
                    type        : string
                    example     : '+11234567890'
                phone-mobile :
                    description : Mobile phone number
                    type        : string
                    example     : '+11234567890'
                phone-work :
                    description : Work phone number
                    type        : string
                    example     : '+11234567890'
                web-site :
                    description : Web site
                    type        : string
                    example     : 'https://www.example.com'
                address :
                    description : Address (can be multiple lines separated by \n)
                    type        : string
                    example     : 123 Main St
                city :
                    description : City
                    type        : string
                    example     : 'Cincinnati'
                county-code :
                    description : County code
                    type        : string
                state-code :
                    $ref : '#/components/schemas/StateCode'
                country-code :
                    $ref : '#/components/schemas/CountryCode'
                postal-code :
                    $ref : '#/components/schemas/PostalCode'
        PersonGetJson :
            allOf :
            -   type : object
                required :
                -   id
                properties :
                    id :
                        $ref : '#/components/schemas/Id'
            -   $ref : '#/components/schemas/PersonData'
        PersonGetJsonApiData :
            type : object
            required :
            -   type
            -   id
            -   attributes
            properties :
                type :
                    type : string
                    enum :
                    -   person
                id :
                    $ref : '#/components/schemas/Id'
                attributes :
                    $ref : '#/components/schemas/PersonData'
        PersonGetJsonApi :
            type : object
            required :
            -   data
            properties :
                jsonapi :
                    $ref : '#/components/schemas/JsonApiInfo'
                data :
                    $ref : '#/components/schemas/PersonGetJsonApiData'
        PersonListJson :
            type : array
            items :
                $ref : '#/components/schemas/PersonGetJson'
        PersonListJsonApi :
            type : object
            required :
            -   data
            properties :
                jsonapi :
                    $ref : '#/components/schemas/JsonApiInfo'
                links :
                    $ref : '#/components/schemas/JsonApiLinks'
                data :
                    type : array
                    items :
                        $ref : '#/components/schemas/PersonGetJsonApiData'
...
