addSbtPlugin("com.beautiful-scala" % "sbt-scalastyle" % "1.5.1")
addSbtPlugin("com.typesafe.play"   % "sbt-plugin"     % "2.8.18")
addSbtPlugin("com.typesafe.sbt"    % "sbt-git"        % "1.0.2")
addSbtPlugin("org.scoverage"       % "sbt-scoverage"  % "1.9.3")

// sbt-web plugins https://github.com/sbt/sbt-web
addSbtPlugin("com.typesafe.sbt"    % "sbt-digest"     % "1.1.4")
addSbtPlugin("io.github.irundaia"  % "sbt-sassify"    % "1.5.2")
